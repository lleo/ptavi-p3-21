#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from urllib.request import urlretrieve
from xml.sax import make_parser

from smil import SMILHandler


class KaraokeLocal:

    def __init__(self, file):
        parser = make_parser()
        sHandler = SMILHandler()
        parser.setContentHandler(sHandler)
        parser.parse(open(file))
        self.info = sHandler.get_tags()
        self.count = 0

    def do_local(self):
        for linea in self.info:
            for attr in linea:
                if attr == "src" and "http://" in linea["src"]:
                    ext = linea["src"].split("/")[-1].split(".")[-1]
                    descarga = f"fichero-{self.count}.{ext}"
                    urlretrieve(linea["src"], descarga)
                    linea["src"] = descarga
                    self.count += 1


def main():
    fichero = (sys.argv[1])
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 karaoke.py file.smil")

    karaoke = KaraokeLocal(fichero)
    karaoke.do_local()


if __name__ == "__main__":
    main()