#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):

    def __init__(self):
        self.lista = []
        self.atributos = {
            "root-layout": ["width", "height", "background-color"],
            "region": ["id", "top", "bottom", "left", "right"],
            "img": ["src", "region", "begin", "dur"],
            "audio": ["src", "begin", "dur"],
            "textstream": ["src", "region"]
            }


    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta

        """
        if name in self.atributos:
            diccionario = {}
            diccionario["etiqueta"] = name
            for atributo in self.atributos[name]:
                diccionario[atributo] = attrs.get(atributo, "")
            self.lista.append(diccionario)

    def get_tags(self):
        return self.lista


if __name__ == "__main__":

    parser = make_parser()
    sHandler = SMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    lista = sHandler.get_tags()