import json
import sys
from urllib.request import urlretrieve
from xml.sax import make_parser

from smil import SMILHandler


class Karaoke:
    def __init__(self, file):
        p = make_parser()
        sHandler = SMILHandler()
        p.setContentHandler(sHandler)
        p.parse(open(file))
        self.info = sHandler.get_tags()
        self.count = 0

    def download(self):
        for linea in self.info:
            for attr in linea:
                if attr == "src" and "http://" in linea["src"]:
                    ext = linea["src"].split("/")[-1].split(".")[-1]
                    descarga = f"fichero-{self.count}.{ext}"
                    urlretrieve(linea["src"], descarga)
                    linea["src"] = descarga
                    self.count += 1

    def __str__(self):
        string = ""
        for tag in self.info:
            for k, v in zip(tag.keys(), tag.values()):
                string = string + f"{k}={v}\t"
            string = string + "\n"
        return string.replace("etiqueta=", "")

    def to_json(self):
        tags_dict = []
        for tag in self.info:
            tag_dict = {"name": tag.get("etiqueta", ""), "attrs": dict((k, tag[k]) for k in list(tag.keys())[1:])}
            tags_dict.append(tag_dict)
            print(json.dumps(tag_dict, indent=1))
        return json.dumps(tags_dict, indent=1)


def main():
    args = sys.argv
    if len(args) <= 1:
        sys.exit("Usage: python3 karaobjecto.py [--json] <file>")

    json_arg = len(list(filter(lambda arg: arg == "--json", args)))
    if json_arg >= 1 and args[1] != "--json":
        sys.exit("Usage: python3 karaobjecto.py [--json] <file>")
    elif len(args) >= 2 and args[1] == "--json":
        fichero = args[2]
        karaoke = Karaoke(fichero)
        print(karaoke)
        karaoke.to_json()
    else:
        fichero = args[1]
        karaoke = Karaoke(fichero)
        print(karaoke)

    karaoke.download()


if __name__ == "__main__":
    main()
