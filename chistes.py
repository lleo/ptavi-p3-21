#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':
            # De esta manera tomamos los valores de los atributos
            print("Empieza chiste")
            self.calificacion = attrs.get('calificacion', "")
            print(self.calificacion)
        elif name == 'pregunta':
            print("Empieza pregunta")
            self.pregunta = ""
            self.inPregunta = True
        elif name == 'respuesta':
            print("Empieza respuesta")
            self.respuesta = ""
            self.inRespuesta = True

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'pregunta':
            print("Termina pregunta")
            self.inPregunta = False
        elif name == 'respuesta':
            print("Termina respuesta")
            self.inRespuesta = False

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """
        if self.inPregunta:
            print("En texto de pregunta")
            self.pregunta = self.pregunta + char
            print(self.pregunta)
        elif self.inRespuesta:
            print("En texto de respuesta")
            self.respuesta += char
            print(self.respuesta)


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes.xml'))


if __name__ == "__main__":
    main()
