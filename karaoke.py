# !/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import sys
from typing import List
from xml.sax import make_parser

from smil import SMILHandler


def to_string(tags: List[dict]):
    string = ""
    for tag in tags:
        for k, v in zip(tag.keys(), tag.values()):
            string = string + f"{k}={v}\t"
        string = string + "\n"
    return string.replace("etiqueta=", "")

# Ejercicio 6
def to_json(tags: List[dict]):
    tags_dict = []
    for tag in tags:
        tag_dict = {"name": tag.get("etiqueta", ""), "attrs": dict((k, tag[k]) for k in list(tag.keys())[1:])}
        tags_dict.append(tag_dict)
        print(json.dumps(tag_dict, indent=1))
    return json.dumps(tags_dict, indent=1)


def main():

    if len(sys.argv) <= 1:
        sys.exit("Usage: python3 karaoke.py file.smil")
    fichero = sys.argv[1]

    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(fichero))

    #Instruccion: python3 karaoke.py karaoke.smil --json
    if len(list(filter(lambda arg: arg == "--json", sys.argv))) >= 1:
        to_json(cHandler.get_tags())
        sys.exit()

    print(to_string(cHandler.get_tags()))


if __name__ == "__main__":
    main()
