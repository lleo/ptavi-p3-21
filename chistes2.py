#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.num = ""
        self.inNum = False

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':
            # De esta manera tomamos los valores de los atributos
            self.calificacion = attrs.get('calificacion', "")

        elif name == 'pregunta':
            self.pregunta = ""
            self.inPregunta = True
        elif name == 'respuesta':
            self.respuesta = ""
            self.inRespuesta = True
        elif name == 'num':
            self.num = ""
            self.inNum = True

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'pregunta':
            self.inPregunta = False
        elif name == 'respuesta':
            self.inRespuesta = False
        elif name == 'num':
            self.inNum = False

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """
        if self.inPregunta:
            self.pregunta = self.pregunta + char
            print("Pregunta: ", self.pregunta)
        elif self.inRespuesta:
            self.respuesta += char
            print("Respuesta: ", self.respuesta, "\n")
        elif self.inNum:
            self.num += char
            print("Chiste número: ", self.num, "(", self.calificacion, ")")


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes2.xml'))


if __name__ == "__main__":
    main()